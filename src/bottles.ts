function capitalize(s: string): string
  { return s.charAt(0).toUpperCase() + s.slice(1); }


export class Bottles {
  public static song() {
    return this.verses(99, 0);
  }

  public static verses(start: number,	end: number) {
    const v: Array<string> = [];
    for (let i = start; i >= end; i--)
      v.push(this.verse(i));
    return v.join('\n');
  }

  public static verse(n: number): string {
    const bottleNum = BottleNumber.for(n);

    return capitalize(`${bottleNum} of beer on the wall, `) +
                      `${bottleNum} of beer.\n` +
           capitalize(`${bottleNum.action()}, `) +
                      `${bottleNum.succ()} of beer on the wall.\n`;
  }
}


export class BottleNumber {
  public static for(n: number): BottleNumber {
    if (n === 0) return new BottleNumber0(n);
    else if (n === 1) return new BottleNumber1(n);
    else if (n === 6) return new BottleNumber6(n);
    else return new BottleNumber(n);
  }
  constructor(protected readonly n: number) { }
  public toString() { return `${this.quantity()} ${this.container()}`; }
  public container(): string { return 'bottles'; }
  public pronoun(): string { return 'one'; }
  public quantity(): string { return this.n.toString(); }
  public succ(): BottleNumber { return BottleNumber.for(this.n - 1); }
  public action(): string { return `take ${this.pronoun()} down and pass it around`; }
}

class BottleNumber0 extends BottleNumber {
  public quantity(): string { return 'no more'; }
  public succ(): BottleNumber { return BottleNumber.for(99); }
  public action(): string { return `go to the store and buy some more`; }
}

class BottleNumber1 extends BottleNumber {
  public container(): string { return 'bottle'; }
  public pronoun(): string { return 'it'; }
}

class BottleNumber6 extends BottleNumber {
  public container(): string { return 'six-pack'; }
  public quantity(): string { return '1'; }
}
